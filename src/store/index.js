import Vue from "vue";
import Vuex from "vuex";
import * as types from "./mutation-types";
import { uid } from "quasar";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

let cart = window.localStorage.getItem("cart");
let cartForUser = window.localStorage.getItem("cartForUser");
let allRecipes = window.localStorage.getItem("allRecipes");
let allRecipesDataStorage = window.localStorage.getItem("allRecipesData");
//get recipes for user
let userId = localStorage.getItem("token");

let recipesforUser = [];
JSON.parse(allRecipesDataStorage) != null
  ? (recipesforUser = JSON.parse(allRecipesDataStorage).filter(i => {
      return i.userId == userId;
    }))
  : [];

let itemInCartforUser = [];
JSON.parse(cart) != null
  ? (itemInCartforUser = JSON.parse(cart).filter(i => {
      return i.userId == userId;
    }))
  : [];

// initial state
const state = {
  allRecipesData: allRecipesDataStorage
    ? JSON.parse(allRecipesDataStorage)
    : [],
  all: allRecipes ? recipesforUser : [],
  added: cart ? JSON.parse(cart) : [],
  itemInCartForUser: cartForUser ? itemInCartforUser : []
};

// getters
const getters = {
  allProducts: state => state.all, // would need action/mutation if data fetched async
  getNumberOfProducts: state => (state.all ? state.all.length : 0),
  cartProducts: state => state.added,
  itemInCartForUser:state=>state.itemInCartForUser,
  //initiat in local storage
  saveToLocalStorage() {
    try {
      const serializedState = JSON.stringify(state);
      localStorage.setItem("state", serializedState);
    } catch (e) {
      console.log(e);
    }
  }
};

// actions
const actions = {
  addToCart({ commit }, product) {
    commit(types.ADD_TO_CART, {
      id: product.id,
      userId: product.userId,
      name: product.name,
      description: product.description,
      image: product.image,
      quantity: 1
    });
  },
  DecreaseItem({ commit }, product) {
    commit(types.Decrease_Item, {
      id: product.id
    });
  },
  addNewRecipe({ commit }, editedItem) {
    commit(types.ADD_New_Recipe, {
      name: editedItem.name,
      description: editedItem.description,
      image: editedItem.image
    });
  },
  UpdateRecipe({ commit }, editedItem) {
    commit(types.Update_Recipe, {
      id: editedItem.id,
      name: editedItem.name,
      description: editedItem.description,
      image: editedItem.image
    });
  },

  DeleteItem({ commit }, id) {
    commit(types.Delete_Item, { id });
  },

  RemoveItemFromCart({ commit }, id) {
    commit(types.Remove_Item_FromCart, { id });
  }
};

// mutations
const mutations = {
  [types.ADD_TO_CART](state, { id, name, description, image, userId }) {
    const record = state.added.find(p => p.id === id);
    if (!record) {
      state.added.push({
        id: id,
        userId: userId,
        name: name,
        description: description,
        image: image,
        quantity: 1
      });
    } else {
      record.quantity++;
    }

    this.commit("saveDataCart");
  },

  [types.Decrease_Item](state, { id }) {
    const record = state.added.find(p => p.id === id);
    record.quantity--;
    this.commit("saveDataCart");
  },
  //save data in local storage
  saveDataCart(state) {

    window.localStorage.setItem("cart", JSON.stringify(state.added));
  },

  //add new recipe
  [types.ADD_New_Recipe](state, { name, description, image }) {
    state.all.push({
      id: uid(),
      userId: localStorage.getItem("token"),
      name: name,
      description: description,
      image: image
    });

    this.commit("saveData");
  },
  //save data in local storage
  saveData(state) {
    window.localStorage.setItem("allRecipes", JSON.stringify(state.all));
    state.allRecipesData.push(state.all[state.all.length - 1]);
    window.localStorage.setItem(
      "allRecipesData",
      JSON.stringify(state.allRecipesData)
    );
  },
  //Update exist recipe
  [types.Update_Recipe](state, { id, name, image, description }) {
    const allRecipes = state.all.find(p => p.id === id);
    (allRecipes.name = name),
      (allRecipes.description = description),
      (allRecipes.image = image);
    localStorage.setItem("allRecipes", JSON.stringify(state.all));

    const allRecipesData = state.allRecipesData.find(p => p.id === id);
    (allRecipesData.name = name),
      (allRecipesData.description = description),
      (allRecipesData.image = image);
    localStorage.setItem(
      "allRecipesData",
      JSON.stringify(state.allRecipesData)
    );
  },

  [types.Delete_Item](state, { id }) {
    // deleteTask(state, id) {
    //recipes for user
    const allRecipes = state.all.filter(item => item.id !== id);
    localStorage.setItem("allRecipes", JSON.stringify(allRecipes));
    state.all = allRecipes;
    //recipes all user
    const allRecipesData = state.allRecipesData.filter(item => item.id !== id);
    localStorage.setItem("allRecipesData", JSON.stringify(allRecipesData));
    state.allRecipesData = allRecipesData;
    // recipes in shopping list
    const cart = state.added.filter(item => item.id !== id);
    localStorage.setItem("cart", JSON.stringify(cart));
    state.added = cart;
  },
  [types.Remove_Item_FromCart](state, { id }) {
    // recipes in shopping list
    const cart = state.added.filter(item => item.id !== id);
    localStorage.setItem("cart", JSON.stringify(cart));
    state.added = cart;
  }
};

// one store for entire application
export default new Vuex.Store({
  state,
  strict: debug,
  getters,
  actions,
  mutations
  // saveToLocalStorage
});
