
const routes = [
  {
    path: '/index',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('components/Recipes/ListRecipes.vue') }
    ]
  },
  {
    path: '/product-details',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('components/Recipes/RecipeDetails.vue') }
    ]
  },
  {
    path: '/',
    component: () => import('layouts/EmptyMainLayout.vue'),
    children: [
      { path: '', component: () => import('components/Auth/SignIn.vue') }
    ]


  },
  {
    path: '/SignUp',
    component: () => import('layouts/EmptyMainLayout.vue'),
    children: [
      { path: '', component: () => import('components/Auth/SignUp.vue') }
    ]


  },
  {
    path: '/Cart',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('components/ShoppingList/Cart.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
